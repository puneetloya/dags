import datetime

from airflow import DAG
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator
from kubernetes.client import models as k8s

with DAG(
        dag_id="streaming_data",
        start_date=datetime.datetime(2023, 7, 17, 20, 0, 0),
        schedule="@once",
):
    ruby_kafka_connector = KubernetesPodOperator(
        name="ruby_kafka_connector",
        image="registry.gitlab.com/k8ai/x3000/ruby-streaming-data-connector:2eb80eda",
        image_pull_secrets=[k8s.V1LocalObjectReference('regcred')],
        cmds=["ruby", "/myapp/ruby_data_connector.rb"],
        configmaps=["k8-configmap"],
        task_id="ruby_kafka_connector",
        max_active_tis_per_dag=1,
        wait_for_downstream=True
    )
