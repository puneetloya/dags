import datetime

from airflow import DAG
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator
from kubernetes.client import models as k8s

with DAG(
        dag_id="push_25k_events",
        #start_date=datetime.datetime(2023, 9, 14, 0, 15, 0),
        start_date=datetime.datetime.now() - datetime.timedelta(hours=0, minutes=1),
        schedule=datetime.timedelta(minutes=1),
):
    generate_demo_data = KubernetesPodOperator(
        name="generate_demo_data",
        image="registry.gitlab.com/k8ai/x3000/examples/demo-data-generator:830f7249",
        image_pull_secrets=[k8s.V1LocalObjectReference('regcred')],
        task_id="generate_demo_data",
        configmaps=["k8-configmap"],
        max_active_tis_per_dag=1,
        wait_for_downstream=True
    )

    sample_csv_connector_s3_s3_copy = KubernetesPodOperator(
        name="sample_csv_connector_s3_s3_copy",
        image="591218250338.dkr.ecr.us-east-2.amazonaws.com/korrel8-pylibrary",
        cmds=["python", "sample_csv_connector_s3_s3_copy.py"],
        task_id="sample_csv_connector_s3_s3_copy",
        configmaps=["k8-configmap"],
        max_active_tis_per_dag=1,
        wait_for_downstream=True
    )

    generate_demo_data.set_downstream(sample_csv_connector_s3_s3_copy)


